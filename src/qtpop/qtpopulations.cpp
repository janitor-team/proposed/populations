#include <qapplication.h>
#include "mainwindow.h"

int main(int argc, char * * argv) {
	QApplication a(argc, argv);

	abif the_data;
	//string filename("../../doc/test.fsa");
	string filename("");
	//string svgfilename("test.svg");
	if (argc == 2) {
		filename = argv[1];
	}
	MainWindow w;
	w.show();

	if (filename != "") {
		the_data.read_abif_file(filename);

		w.addAbif(the_data);
	}

	return a.exec();
}
