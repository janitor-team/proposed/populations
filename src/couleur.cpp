/***************************************************************************
                          couleur.cpp  -  description
                             -------------------
    begin                : Mon Jun 4 2001
    copyright            : (C) 2001 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#include "couleur.h"

vector<string> Couleur::_tab_couleurs;
unsigned int Couleur::_pos_tab_couleurs(0);

Couleur::Couleur(unsigned long rouge, unsigned long vert, unsigned long bleu) {
	_boolrgb = true;
	_boolnom = false;

	_rouge = rouge;
	_vert= vert;
	_bleu = bleu;
}

Couleur::Couleur(const biolib::vecteurs::ChaineCar & couleur){
	//couleur peut contenir 3 chiffres (rgb) ou un nom de couleur en Anglais
	biolib::vecteurs::ChaineCar mot, lacouleur(couleur);

	_boolrgb = false;
	_boolnom = false;

	if (lacouleur == "") lacouleur.assign(get_couleur_diff());

	if (lacouleur.GetNbMots() == 3) {
		_boolrgb = true;
		lacouleur.GetMot(1, mot);
		if (mot.EstUnChiffre()) {
			_rouge = (int) mot;
			_rouge *= 256;
		}
		else _boolrgb = false;
		lacouleur.GetMot(2, mot);
		if ((_boolrgb) && (mot.EstUnChiffre())) {
			_vert = (int) mot;
			_vert *= 256;
		}
		else _boolrgb = false;
		lacouleur.GetMot(3, mot);
		if ((_boolrgb) && (mot.EstUnChiffre())) {
			_bleu = (int) mot;
			_bleu *= 256;
		}
		else _boolrgb = false;
		
	}
	if (_boolrgb == false) {//nom de couleur en Anglais
		_boolnom = true;
		_nom.assign(lacouleur);
	}

}

Couleur::~Couleur(){
}

bool Couleur::operator== (const Couleur &rval) const {
	if ((_boolrgb) && (rval._boolrgb) && (_rouge == rval._rouge) && (_vert == rval._vert) && (_bleu == rval._bleu)) return true;

	if ((_boolnom) && (rval._boolnom) && (_nom == rval._nom)) return true;

	return false;
}

const string & Couleur::get_couleur_diff() {
	unsigned int i;

	if ( Couleur::_tab_couleurs.size() == 0 ) Couleur::f_rempli_tab_couleurs();
	i = Couleur::_pos_tab_couleurs;
	Couleur::_pos_tab_couleurs++;
	if (Couleur::_pos_tab_couleurs == Couleur::_tab_couleurs.size()) Couleur::_pos_tab_couleurs = 0;

	return (Couleur::_tab_couleurs[i]);
}


void Couleur::f_rempli_tab_couleurs() {

	_tab_couleurs.push_back("red");
	_tab_couleurs.push_back("green");
	_tab_couleurs.push_back("blue");
	_tab_couleurs.push_back("turquoise");
	_tab_couleurs.push_back("pink");
	_tab_couleurs.push_back("yellow");
	_tab_couleurs.push_back("brown");
	_tab_couleurs.push_back("purple");
	_tab_couleurs.push_back("sea green");
	_tab_couleurs.push_back("salmon");
	_tab_couleurs.push_back("sky blue");
	_tab_couleurs.push_back("yellow green");
	_tab_couleurs.push_back("brown1");
	_tab_couleurs.push_back("aquamarine");
	_tab_couleurs.push_back("orange");
	_tab_couleurs.push_back("brown2");
	_tab_couleurs.push_back("navy blue");
	_tab_couleurs.push_back("goldenrod");
	_tab_couleurs.push_back("tomato");
	_tab_couleurs.push_back("violet");
	_tab_couleurs.push_back("gold");

}
