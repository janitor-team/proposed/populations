/***************************************************************************
                          jeupopexp.h  -  Classe dérivée de Jeupop, spécialisée
															dans l'exportation des données
                             -------------------
    begin                : Tue Oct 24 2000
    copyright            : (C) 2000 by Olivier Langella
    email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef JEUPOPEXP_H
#define JEUPOPEXP_H

//#include <String.h>
#include <iomanip>
#include "jeupop.h"
#include "vecteurs.h"

/**
  *@author Olivier Langella
  */

class JeuPopExp {
public: 
	JeuPopExp(Jeupop *);
	~JeuPopExp();

	void oLea(unsigned long P1,unsigned long P2,unsigned long H, ostream & sortie, ostream & infos);
  /** Exportation au format "admix" de G. Bertorelle
voir le site:
http://www.unife.it/genetica/Giorgio/giorgio.html
 */
  void oAdmix_dat(unsigned long P1,unsigned long P2,unsigned long H, ostream & sortie, ostream & infos);
  void oAdmix_mtx(ostream & sortie, ostream & infos);
	void oGenetix(ostream & sortie, ostream & infos);
	void oFstat(ostream & sortie, ostream & infos);
  void oPopulationsXML(ostream & sortie, ostream & infos) const;

private:
	const Jeupop * _Pjeu;
	Jeupop * _Pcopie;
  /** Prépare une copie de jeupop avec 3
populations P1, P2, H (special admixture) */
  void f_prepP1P2H(unsigned long P1,unsigned long P2,unsigned long H, ostream & infos);

public:
	struct Anomalie{

		int le_pb;
		Anomalie (int i, string format):le_pb(i), _format(format){};
		string _message;
		string _format;

		string& fmessage(int num){
			switch (num) {
			case 1:
				_message = _("Error n°1 in JeuPopExp while exporting to ");
				_message += _format;
				break;
			case 2:
				_message = _("Error n°2 in JeuPopExp: incompatible alleles names to export in ");
				_message += _format;
				break;
			case 100:
				_message = _("Error n°100 in JeuPopExp: ");
				_message += _format;
				_message += _(" require diploid populations");
				break;
				
			case 1000:
				_message = _("Error n°1000 in JeuPopExp: locus names do not exceed 5 caracters in ");
				_message += _format;
				break;
			case 1001:
				_message = _("Error n°1001 in JeuPopExp: individual names do not exceed 10 caracters in ");
				_message += _format;
				break;
			default:
				_message = _("Error in JeuPopExp");
				break;
			}
			return(_message);
		}
	};
	
};

#endif

