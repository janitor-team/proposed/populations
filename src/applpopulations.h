/***************************************************************************
 applpopulations.h  -  description
 -------------------
 begin                : Mon Oct 30 2000
 copyright            : (C) 2000 by Olivier Langella
 email                : Olivier.Langella@pge.cnrs-gif.fr
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef APPLPOPULATIONS_H
#define APPLPOPULATIONS_H

#include "config.h"
#define version_populations POPULATIONS_VERSION

#include "arbreplus.h"
#include "fstat.h"
#include "applpop.h"
# ifndef COMPILATION_MINGW32
//# include "olivxml.h"
# endif

//#include "distgnt.h"
/**Objet sp�cifique � l'application "Populations"
 *@author Olivier Langella
 */
typedef biolib::arbres::ArbrePlus ArbrePlus;
typedef biolib::vecteurs::ChaineCar ChaineCar;
//typedef biolib::arbres::Arbre Arbre;

class ApplPopulations: public ApplPop {
public:
	ApplPopulations();
	~ApplPopulations();

	void affPubEntree() const;
	/**  */
	void affVlocus(const Vecteur<unsigned int> &) const;
	/** Demande le nom ou le numero d'un locus */
	int DemandeLocus(const char laquestion[] = 0) const;

private:

	int menu_principal();
	void menu_formats();
	int menu_metdistpop();
	//	int menu_metdistind();
	int menu_metconstructarbre();
	void menu_popstructurees();
	void menu_arbrepop();
	void menu_arbreind();
	void menu_calculs();
	/** Choix de locus */
	Vecteur<unsigned int> menu_choixlocus() const;

	//traitement en ligne de commande:
	void fLigneCommande(char ** commandes, int nbcommandes);
	void fLiPhylogeny(Titre & tab_commandes);
	void fLiHelp() const;
	void fLiTest(Titre & tab_commandes);

	void fconstructarbre(MatriceLD &distances, string & nomficarbre,
			int methodearbre);
	void fmat2arbre();
	bool fcompdistind(MatriceLD &distances, int metdist = 0);
	void fdistind();
	void ftreeind();
	void ftreeindBootLocus();
	bool fcompdistpop(MatriceLD &distances, int metdist = 0);
	void fdistpop();
	void ftreepop();
	void ftreepopBootLocus();
	void ftreepopBootInd();
	void fintrogresse();
	void frarefaction();
	void fsetniveau();
	void fmicrosatcorrections();

private:

	void ecritGenepop();
	void ecritGenepopCano();
	void ecritPopulations();
	void ecritImmanc();
	void ecritMsat(int);
	void ecritArbre(ArbrePlus & larbre, string nomFichier = "");
	void ecritLea();
	void ecritAdmix();
	void ecritGenetix();
	void ecritFstat();
	void ecritRapportFst();
	void ecritPopulationsXML();

	void AffNiveauxPopStruc(ostream &);

	Vecteur<unsigned int> _VcalcLocus;
	bool _square_distance;
};

#endif

