<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.5//EN"
"http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd" [

<!--

`xsltproc -''-nonet \
          -''-param man.charmap.use.subset "0" \
          -''-param make.year.ranges "1" \
          -''-param make.single.year.ranges "1" \
          /usr/share/xml/docbook/stylesheet/docbook-xsl/manpages/docbook.xsl \
          manpage.xml'

A manual page <package>.<section> will be generated. You may view the
manual page with: nroff -man <package>.<section> | less'. A typical entry
in a Makefile or Makefile.am is:

DB2MAN = /usr/share/sgml/docbook/stylesheet/xsl/docbook-xsl/manpages/docbook.xsl
XP     = xsltproc -''-nonet -''-param man.charmap.use.subset "0"

manpage.1: manpage.xml
        $(XP) $(DB2MAN) $<

The xsltproc binary is found in the xsltproc package. The XSL files are in
docbook-xsl. A description of the parameters you can use can be found in the
docbook-xsl-doc-* packages. Please remember that if you create the nroff
version in one of the debian/rules file targets (such as build), you will need
to include xsltproc and docbook-xsl in your Build-Depends control field.
Alternatively use the xmlto command/package. That will also automatically
pull in xsltproc and docbook-xsl.

Notes for using docbook2x: docbook2x-man does not automatically create the
AUTHOR(S) and COPYRIGHT sections. In this case, please add them manually as
<refsect1> ... </refsect1>.

To disable the automatic creation of the AUTHOR(S) and COPYRIGHT sections
read /usr/share/doc/docbook-xsl/doc/manpages/authors.html. This file can be
found in the docbook-xsl-doc-html package.

Validation can be done using: `xmllint -''-noout -''-valid manpage.xml`

General documentation about man-pages and man-page-formatting:
man(1), man(7), http://www.tldp.org/HOWTO/Man-Page/

-->

  <!-- Fill in your name for FIRSTNAME and SURNAME. -->
  <!ENTITY dhfirstname "Georges">
  <!ENTITY dhsurname   "Khaznadar">
  <!-- dhusername could also be set to "&dhfirstname; &dhsurname;". -->
  <!ENTITY dhusername  "Georges Khaznadar">
  <!ENTITY dhemail     "georgesk@ofset.org">
  <!-- SECTION should be 1-8, maybe w/ subsection other parameters are
       allowed: see man(7), man(1) and
       http://www.tldp.org/HOWTO/Man-Page/q2.html. -->
  <!ENTITY dhsection   "1">
  <!-- TITLE should be something like "User commands" or similar (see
       http://www.tldp.org/HOWTO/Man-Page/q2.html). -->
  <!ENTITY dhtitle     "populations User Manual">
  <!ENTITY dhucpackage "POPULATIONS">
  <!ENTITY dhpackage   "populations">
]>

<refentry>
  <refentryinfo>
    <title>&dhtitle;</title>
    <productname>&dhpackage;</productname>
    <authorgroup>
      <author>
       <firstname>&dhfirstname;</firstname>
        <surname>&dhsurname;</surname>
        <contrib>Wrote this manpage for the Debian system.</contrib>
        <address>
          <email>&dhemail;</email>
        </address>
      </author>
    </authorgroup>
    <copyright>
      <year>2011</year>
      <holder>&dhusername;</holder>
    </copyright>
    <legalnotice>
      <para>This manual page was written for the Debian system
        (and may be used by others).</para>
      <para>Permission is granted to copy, distribute and/or modify this
        document under the terms of the GNU General Public License,
        Version 2 or (at your option) any later version published by
        the Free Software Foundation.</para>
      <para>On Debian systems, the complete text of the GNU General Public
        License can be found in
        <filename>/usr/share/common-licenses/GPL</filename>.</para>
    </legalnotice>
  </refentryinfo>
  <refmeta>
    <refentrytitle>&dhucpackage;</refentrytitle>
    <manvolnum>&dhsection;</manvolnum>
  </refmeta>
  <refnamediv>
    <refname>&dhpackage;</refname>
    <refpurpose>population genetic software</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <cmdsynopsis>
      <command>&dhpackage;</command>
    </cmdsynopsis>
    <cmdsynopsis>
      <command>&dhpackage;</command>
      <arg><option><replaceable>name_of_input_file</replaceable></option></arg>       <arg><option><replaceable>option</replaceable></option></arg>    </cmdsynopsis>
      <para>You can use <command>&dhpackage;</command> as a command line program (very useful for batch treatment) to infer phylogenetic trees.</para>
  </refsynopsisdiv>
  <refsect1 id="description">
    <title>DESCRIPTION</title>
    <para><command>&dhpackage;</command> is a population genetic software. It computes genetic
 distances between populations or individuals. It builds phylogenetic
 trees (NJ or UPGMA) with bootstrap values.</para>
  </refsect1>
  <refsect1>
    <title>Features</title>
    <itemizedlist>
      <listitem><para>haploids, diploids or polyploids genotypes (see input formats)</para></listitem>
      <listitem><para>structured populations (see input files structured populations</para></listitem>
      <listitem><para>No limit of populations, loci, alleles per loci (see input formats)</para></listitem>
      <listitem><para>Distances between individuals (15 different methods)</para></listitem>
      <listitem><para>Distances between populations (15 methods)</para></listitem>
      <listitem><para>Bootstraps on loci OR individuals</para></listitem>
      <listitem><para>Phylogenetic trees (individuals or populations), using Neighbor Joining or UPGMA (PHYLIP tree format)</para></listitem>
      <listitem><para>Allelic diversity</para></listitem>
      <listitem><para>Converts data files from Genepop to different formats (Genepop, Genetix, Msat, Populations...)</para></listitem>
    </itemizedlist>
  </refsect1>
  <refsect1>
    <title>OPTIONS</title>
    <variablelist>
      <varlistentry>
	<term><option>-phylogeny</option> <replaceable>ind|pop</replaceable></term>
	<listitem><para>(default) for phylogenetic trees based on individuals or populations
	</para></listitem>
      </varlistentry>
      <varlistentry>
	<term><option>dist</option> <replaceable>method</replaceable></term>
	<listitem><para>(default: Nei standard, Ds) you can choose among: DAS, Dm, Ds, Dc, Da, dmu2, Fst, Cp, Dr, ASD, Dsw, Dr, Dru, Drw, Drl. see distances for details.
	</para></listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-construct</option> <replaceable>method</replaceable></term>
	<listitem><para>(default: upgma) possibilities upgma or nj (Neighbor Joining)
	</para></listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-bootstrap_ind</option> <replaceable>n</replaceable></term>
	<listitem><para>number to indicate the number of bootstraps to perform on individuals
	</para></listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-bootstrap_locus</option> <replaceable>n</replaceable></term>
	<listitem><para>
	</para></listitem>number to indicate the number of bootstraps to perform on loci
      </varlistentry>
      <varlistentry>
	<term><option>-output</option> <replaceable>name_of_treeview_file</replaceable></term>
	<listitem><para>to indicate the name of the tree file (phylip tree format)
	</para></listitem>
      </varlistentry>
      <varlistentry>
	<term><option>-level</option> <replaceable>n</replaceable></term>
	<listitem><para>number , structured populations allows to choose the structuration factor (in the example: town level is 1, building level is 2...).
	</para></listitem>
      </varlistentry>
    </variablelist>
  </refsect1>
  <refsect1>
    <title>EXAMPLE</title>
    <para><command>populations toutc2.txt -phylogeny pop -dist Dm -bootstrap_locus 10000 -output toutc2_10000_Dm.tre</command></para>
    <para>Commands can be write in a .bat file (for DOS) or a script file (for UNIX)</para>
  </refsect1>
</refentry>

